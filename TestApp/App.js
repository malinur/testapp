import 'react-native-gesture-handler';
import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';

import ListingsScreen from './app/screens/ListingsScreen';
import ViewImageScreen from './app/screens/ViewImageScreen';
import Navigator from './app/navigation/Navigator';

export default function App() {
  return <Navigator />;
}

const styles = StyleSheet.create({});
