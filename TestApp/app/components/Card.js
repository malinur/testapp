import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';

export default function Card({title, subtitle, image, onPress}) {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.card}>
        <Image style={styles.image} source={image} />
        <View style={styles.detailsContainer}>
          <Text style={styles.title} numberOfLines={1}>
            {title}
          </Text>
          <Text style={styles.subTitle} numberOfLines={2}>
            {subtitle}
          </Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  card: {
    borderRadius: 15,
    backgroundColor: 'white',
    marginBottom: 20,
    overflow: 'hidden',
    flexDirection: 'row',
  },
  detailsContainer: {
    padding: 20,
  },
  image: {
    width: 100,
    height: 100,
  },
  subtitle: {
    color: '#d2d2d2',
    fontWeight: 'bold',
  },
  title: {
    color: '#0c0c0c',
    marginBottom: 7,
  },
});
