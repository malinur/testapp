import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';

export default function ViewImageScreen({route}) {
  const listing = route.params;

  return (
    <View style={styles.container}>
      <Image resizeMode="contain" style={styles.image} source={listing.image} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    flex: 1,
  },

  image: {
    width: '100%',
    height: '80%',
  },
});
