import React from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import Card from '../components/Card';
const listings = [
  {
    id: 1,
    author: 'Red jacket for sale',
    name: 'LALALLLA',
    image: require('../assets/jacket.jpg'),
  },
  {
    id: 2,
    author: 'Couch in great condition',
    name: 'lalalalalallala',
    image: require('../assets/couch.jpg'),
  },
];

export default function ListingsScreen({navigation}) {
  return (
    <FlatList
      style={styles.screen}
      data={listings}
      keyExtractor={(listing) => listing.id.toString()}
      renderItem={({item}) => (
        <Card
          title={item.name}
          subtitle={item.author}
          image={item.image}
          onPress={() => navigation.navigate('Photo', item)}
        />
      )}
    />
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 20,
    backgroundColor: '#f8f4f4',
  },
});
